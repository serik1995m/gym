@extends('admin.layouts.admin')

@section('content')

    @parent

    <div class="row">
        <div class="col-md-6">
            <div class="box box-default">

                <div class="box-header with-border">
                    <h1 class="box-title">

                        @if(isset($subscription))
                            Редактировать абонимент
                        @else
                            Добавить абонимент
                        @endif

                    </h1>
                </div>
                @if($types->count())
                    <form action="{{ route('admin.subscription.save') }}" method="POST">

                        @csrf

                        <div class="box-body">

                            @if(isset($subscription))
                                <input type="hidden" name="id" value="{{ $subscription->id }}">
                            @endif

                            <div class="form-group">
                                <label for="status">Тип абонимента</label>
                                <select id="status" class="form-control" name="type_id">
                                    @foreach($types as $type)
                                        <option value="{{ $type->id }}" {{ (isset($subscription) && $subscription->type_id == $type->id) ? 'selected' : '' }}>{{ $type->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="name">Название</label>
                                <input id="name" type="text" class="form-control" name="name"
                                       value="{{ (old('name') ?? isset($subscription) ? $subscription->name : '') }}">
                            </div>

                            <div class="form-group">
                                <label for="price">Цена</label>
                                <input id="price" type="number" class="form-control" name="price"
                                       value="{{ (old('price') ?? isset($subscription) ? $subscription->price : '') }}">
                            </div>

                            <div class="form-group">
                                <label for="count_days">Количество дней</label>
                                <input id="count_days" type="number" class="form-control" name="count_days"
                                       value="{{ (old('count_days') ?? isset($subscription) ? $subscription->count_days : '') }}">
                            </div>

                            <div class="form-group">
                                <label for="number_trainings">Количество тренировок</label>
                                <input id="number_trainings" type="number" class="form-control" name="number_trainings"
                                       value="{{ (old('number_trainings') ?? isset($subscription) ? $subscription->number_trainings : '') }}">
                            </div>

                            <div class="form-group">

                                <div class="checkbox">
                                    <label>
                                        <input type="hidden" name="status" value="0">
                                        <input type="checkbox" name="status"
                                               {{ (isset($subscription) && $subscription->status == 1)  ? 'checked' : '' }} value="1">
                                        <strong>Активный</strong>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="box-footer">

                            @if(isset($subscription))
                                <a href="{{ route('admin.subscription.delete', ['id' => $subscription->id]) }}"
                                   class="btn btn-danger">Удалить</a>
                            @endif

                            <button type="submit" class="btn btn-success pull-right">Сохранить</button>

                        </div>

                    </form>

                @else
                    <div class="box-body">
                        <p class="text-danger">У вас не типов!</p>
                        <a href="{{ route('admin.type.create') }}" class="btn btn-primary">Добавить тип</a>
                    </div>
                @endif

            </div>
        </div>
    </div>
@endsection