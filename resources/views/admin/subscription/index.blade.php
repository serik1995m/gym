@extends('admin.layouts.admin')

@section('content')
    @parent

    <div class="row">
        <div class="col-md-6">
            <div class="box box-default">

                <div class="box-header with-border">
                    <h1 class="box-title">Абонименты</h1>
                </div>

                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_1" data-toggle="tab" aria-expanded="true">Активные</a>
                        </li>
                        <li>
                            <a href="#tab_2" data-toggle="tab" aria-expanded="false">Не активные</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <table class="table table-bordered table-striped dataTable">
                                <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Имя</th>
                                    <th>Цена</th>
                                    <th>Количество дней</th>
                                    <th>Количество тренировок</th>
                                    <th class="col-md-2"></th>
                                </tr>
                                </thead>
                                <tbody>

                                @if($subscriptions_active->count())

                                    @foreach($subscriptions_active as $subscription_active)
                                        <tr>
                                            <td>{{ $subscription_active->id }}</td>
                                            <td>{{ $subscription_active->name }}</td>
                                            <td>{{ $subscription_active->price }} грн.</td>
                                            <td>{{ $subscription_active->count_days }}</td>
                                            <td>{{ $subscription_active->number_trainings }}</td>
                                            <td>
                                                <a href="{{ route('admin.subscription.edit', ['id' => $subscription_active->id]) }}">Редактировать</a>
                                            </td>
                                        </tr>
                                    @endforeach

                                @else
                                    <tr>
                                        <td colspan="4">
                                            Абониментов не найдено!
                                        </td>
                                    </tr>
                                @endif

                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <table class="table table-bordered table-striped dataTable">
                                <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Имя</th>
                                    <th>Цена</th>
                                    <th>Количество дней</th>
                                    <th>Количество тренировок</th>
                                    <th class="col-md-2"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($subscriptions_not_active->count())

                                    @foreach($subscriptions_not_active as $subscription_not_active)
                                        <tr>
                                            <td>{{ $subscription_not_active->id }}</td>
                                            <td>{{ $subscription_not_active->name }}</td>
                                            <td>{{ $subscription_not_active->price }} грн.</td>
                                            <td>{{ $subscription_not_active->count_days }}</td>
                                            <td>{{ $subscription_not_active->number_trainings }}</td>
                                            <td>
                                                <a href="{{ route('admin.subscription.edit', ['id' => $subscription_not_active->id]) }}">Редактировать</a>
                                            </td>
                                        </tr>
                                    @endforeach

                                @else
                                    <tr>
                                        <td colspan="4">
                                            Абониментов не найдено!
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="box-body">

                    <a href="{{ route('admin.subscription.create') }}" class="btn btn-primary pull-right">Добавить новый абонимент</a>

                </div>
            </div>
        </div>
    </div>
@endsection