@extends('admin.layouts.admin')

@section('content')
    @parent

    <div class="row">
        <div class="col-md-6">
            <div class="box box-default">

                <div class="box-header with-border">
                    <h1 class="box-title">Добавить тип</h1>
                </div>

                <form action="{{ route('admin.type.update') }}" method="POST">
                    <div class="box-body">

                        @csrf

                        <input type="hidden" value="{{ $type->id }}" name="id">

                        <div class="form-group">
                            <label for="name">Название</label>
                            <input id="name" type="text" class="form-control" name="name" value="{{ $type->name }}">
                        </div>

                        <div class="form-group">
                            <label for="description">Описание</label>
                            <textarea id="description" class="form-control" name="description" rows="3">{{ $type->description }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="status">Статус</label>
                            <select id="status" class="form-control" name="status">
                                <option value="1" @if($type->status == 1) selected @endif>Активный</option>
                                <option value="0" @if($type->status == 0) selected @endif>Не активный</option>
                            </select>
                        </div>

                    </div>

                    <div class="box-footer">

                        <a href="{{ route('admin.type.delete', ['id' => $type->id]) }}" class="btn btn-danger">Удалить</a>
                        <button class="btn btn-primary pull-right" type="submit">Сохранить</button>

                    </div>

                </form>

            </div>
        </div>
    </div>
@endsection