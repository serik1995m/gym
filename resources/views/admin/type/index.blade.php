@extends('admin.layouts.admin')

@section('content')
    @parent

    <div class="row">
        <div class="col-md-6">
            <div class="box box-default">

                <div class="box-header with-border">
                    <h1 class="box-title">Типы абониментов</h1>
                </div>

                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_1" data-toggle="tab" aria-expanded="true">Активные</a>
                        </li>
                        <li>
                            <a href="#tab_2" data-toggle="tab" aria-expanded="false">Не активные</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <table class="table table-bordered table-striped dataTable">
                                <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Имя</th>
                                    <th>Описание</th>
                                    <th class="col-md-2"></th>
                                </tr>
                                </thead>
                                @if($types_active->count())

                                    <tbody>

                                    @foreach($types_active as $type_active)
                                        <tr>
                                            <td>{{ $type_active->id }}</td>
                                            <td>{{ $type_active->name }}</td>
                                            <td>{{ $type_active->description }}</td>
                                            <td>
                                                <a href="{{ route('admin.type.edit', ['id' => $type_active->id]) }}">Редактировать</a>
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>

                                @endif

                            </table>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <table class="table table-bordered table-striped dataTable">
                                <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Имя</th>
                                    <th>Описание</th>
                                    <th class="col-md-2"></th>
                                </tr>
                                </thead>
                                @if($types_not_active->count())

                                    <tbody>

                                    @foreach($types_not_active as $type_not_active)
                                        <tr>
                                            <td>{{ $type_not_active->id }}</td>
                                            <td>{{ $type_not_active->name }}</td>
                                            <td>{{ $type_not_active->description }}</td>
                                            <td>
                                                <a href="{{ route('admin.type.edit', ['id' => $type_not_active->id]) }}">Редактировать</a>
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>

                                @endif
                            </table>
                        </div>
                    </div>
                </div>

                <div class="box-body">

                    <a href="{{ route('admin.type.create') }}" class="btn btn-primary pull-right">Добавить новый тип</a>

                </div>
            </div>
        </div>
    </div>
@endsection