@extends('admin.layouts.admin')

@section('content')
    @parent

    <div class="row">
        <div class="col-md-6">
            <div class="box box-default">

                <div class="box-header with-border">
                    <h1 class="box-title">Добавить тип</h1>
                </div>

                <form action="{{ route('admin.type.save') }}" method="post">

                    @csrf

                    <div class="box-body">

                        <div class="form-group">
                            <label for="name">Название</label>
                            <input id="name" type="text" class="form-control" name="name">
                        </div>

                        <div class="form-group">
                            <label for="description">Описание</label>
                            <textarea id="description" class="form-control" rows="3" name="description"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="status">Статус</label>
                            <select id="status" class="form-control" name="status">
                                <option value="1">Активный</option>
                                <option value="0">Не активный</option>
                            </select>
                        </div>

                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary pull-right">Сохранить</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection