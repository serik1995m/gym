@extends('adminlte::page')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
@stop


@section('content')

    @if (session('success')) <div class="callout callout-success"> {{ session('success') }} </div> @endif
    @if (session('error')) <div class="callout callout-danger"> {{ session('error') }} </div> @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

@endsection

@section('js')
    <script src="{{ asset('js/jquery.inputmask.js') }}" defer></script>
    <script src="{{ asset('js/common.js') }}" defer></script>
@stop