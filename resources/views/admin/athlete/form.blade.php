@extends('admin.layouts.admin')

@section('content')

    @parent

    <div class="row">
        <div class="col-md-6">
            <div class="box box-default">

                <div class="box-header with-border">
                    <h1 class="box-title">

                        @if(isset($athlete))
                            Редактировать атлета
                        @else
                            Добавить атлета
                        @endif

                    </h1>
                </div>
                <form action="{{ route('admin.athlete.save') }}" method="POST">

                    @csrf

                    <div class="box-body">

                        @if(isset($athlete))
                            <input type="hidden" name="id" value="{{ $athlete->id }}">
                        @endif

                        <div class="form-group">
                            <label for="surname">Фамилия</label>
                            <input id="surname" type="text" class="form-control" name="surname"  value="{{ (old('surname') ?? isset($athlete) ? $athlete->surname : '') }}">
                        </div>

                        <div class="form-group">
                            <label for="name">Имя</label>
                            <input id="name" type="text" class="form-control" name="name"  value="{{ (old('name') ?? isset($athlete) ? $athlete->name : '') }}">
                        </div>

                        <div class="form-group">
                            <label for="patronymic">Отчество</label>
                            <input id="patronymic" type="text" class="form-control" name="patronymic"  value="{{ (old('patronymic') ?? isset($athlete) ? $athlete->patronymic : '') }}">
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input id="email" type="email" class="form-control" name="email"  value="{{ (old('email') ?? isset($athlete) ? $athlete->email : '') }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="phone">Телефон</label>
                                    <input id="phone" type="tel" class="form-control" name="phone"  value="{{ (old('phone') ?? isset($athlete) ? $athlete->phone : '') }}">
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="box-footer">

                        <button type="submit" class="btn btn-success pull-right">Сохранить</button>

                    </div>

                </form>



            </div>
        </div>
    </div>

@endsection