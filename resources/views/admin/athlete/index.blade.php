@extends('admin.layouts.admin')

@section('content')

    @parent

    <div class="box box-primary">

        <div class="box-header">
            <h3 class="box-title"> Атлеты</h3>&nbsp
            <a href="{{ route('admin.athlete.create') }}" class="btn btn-primary">Добавить атлета</a>
            <a href="{{ route('admin.athletes.index') }}" class="btn btn-danger pull-right">Сбросить фильтр</a>
        </div>

        <div class="box-body">
            <div class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                    <div class="col-sm-12">
                        <form id="table-filter" action="{{ route('admin.athletes.index') }}" method="get"></form>
                        <table class="table table-bordered table-striped dataTable dataTableFiltration" role="grid">
                            <thead>
                                <tr>
                                    <th class="col-md-1">id</th>
                                    <th class="col-md-2">Имя</th>
                                    <th class="col-md-2">Фамилия</th>
                                    <th class="col-md-2">Отчество</th>
                                    <th class="col-md-1">Email</th>
                                    <th class="col-md-2">Телефон</th>
                                    <th class="col-md-1">Роль</th>
                                    <th class="col-md-1"></th>
                                </tr>
                                <tr>
                                    <th>
                                        <label>
                                            <input class="form-control" type="number" name="id" value="{{ request('id') }}" form="table-filter">
                                        </label>
                                    </th>
                                    <th>
                                        <label>
                                            <input class="form-control" type="text" name="name" value="{{ request('name') }}" form="table-filter">
                                        </label>
                                    </th>
                                    <th>
                                        <label>
                                            <input class="form-control" type="text" name="surname" value="{{ request('surname') }}" form="table-filter">
                                        </label>
                                    </th>
                                    <th>
                                        <label>
                                            <input class="form-control" type="text" name="patronymic" value="{{ request('patronymic') }}" form="table-filter">
                                        </label>
                                    </th>
                                    <th>
                                        <label>
                                            <input class="form-control" type="email" name="email" value="{{ request('email') }}" form="table-filter">
                                        </label>
                                    </th>
                                    <th>
                                        <label>
                                            <input class="form-control" type="tel" name="phone" value="{{ request('phone') }}" form="table-filter">
                                        </label>
                                    </th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>

                            <tbody>
                                @if($athletes->count())

                                    @foreach($athletes as $athlete)
                                        <tr>
                                            <td tabindex="0" rowspan="1" colspan="1">{{ $athlete->id }}</td>
                                            <td tabindex="0" rowspan="1" colspan="1">{{ $athlete->name }}</td>
                                            <td tabindex="0" rowspan="1" colspan="1">{{ $athlete->surname }}</td>
                                            <td tabindex="0" rowspan="1" colspan="1">{{ $athlete->patronymic }}</td>
                                            <td tabindex="0" rowspan="1" colspan="1">{{ $athlete->email }}</td>
                                            <td tabindex="0" rowspan="1" colspan="1">{{ $athlete->phone }}</td>
                                            <td>{{ $athlete->role->name }}</td>
                                            <td>
                                                <a href=""><i class="fa fa-eye"></i></a>&nbsp
                                                <a href=""><i class="fa fa-pencil-square-o"></i></a>&nbsp
                                            </td>
                                        </tr>
                                    @endforeach

                                @else
                                    <tr>
                                        <td colspan="6">
                                            Атлетов не найдено
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        @if($athletes->count())

            <div class="box-footer">
                {{ $athletes->links() }}
            </div>

        @endif

    </div>

@endsection