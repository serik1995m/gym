<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
class SubscriptionSeeder extends Seeder
{


    private $subscriptions = [
        [
            'type_id' => 1,
            'name' => 'Безлим',
            'price' => 400,
            'count_days' => null,
            'number_trainings' => null,
            'status' => 1,
        ],
        [
            'type_id' => 2,
            'name' => 'Утрений',
            'price' => 150,
            'count_days' => 40,
            'number_trainings' => 12,
            'status' => 1,
        ],
        [
            'type_id' => 3,
            'name' => 'Вечерний',
            'price' => 220,
            'count_days' => 40,
            'number_trainings' => 12,
            'status' => 0,
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->subscriptions as $subscription) {
            DB::table('subscriptions')->insert([
                'type_id' => $subscription['type_id'],
                'name' => $subscription['name'],
                'price' => $subscription['price'],
                'count_days' => $subscription['count_days'],
                'number_trainings' => $subscription['number_trainings'],
                'status' => $subscription['status'],
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
