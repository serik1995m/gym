<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Carbon;

class TypeSeeder extends Seeder
{
    private $types = [
        'Безлим',
        'Утрений',
        'Вечерний',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->types as $type) {
            \DB::table('types')->insert([
                'name' => $type,
                'status' => true,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
