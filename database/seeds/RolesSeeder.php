<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->updateOrInsert(['id' => 1], [
            'name' => 'Администратор',
            'key' => 'admin',
            'news_create' => true,
            'news_edit' => true,
            'news_moderation' => true,
            'type_create' => true,
            'type_edit' => true,
            'subscription_create' => true,
            'subscription_edit' => true,
            'user_create' => true,
            'user_edit' => true,
            'role_management' => true,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('roles')->updateOrInsert(['id' => 2], [
            'name' => 'Атлет',
            'key' => 'athlete',
            'news_create' => false,
            'news_edit' => false,
            'news_moderation' => false,
            'type_create' => false,
            'type_edit' => false,
            'subscription_create' => false,
            'subscription_edit' => false,
            'user_create' => false,
            'user_edit' => false,
            'role_management' => false,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}