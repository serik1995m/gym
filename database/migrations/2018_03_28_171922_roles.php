<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Roles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('key')->unique();
            $table->boolean('news_create')->default(false);
            $table->boolean('news_edit')->default(false);
            $table->boolean('news_moderation')->default(false);
            $table->boolean('type_create')->default(false);
            $table->boolean('type_edit')->default(false);
            $table->boolean('subscription_create')->default(false);
            $table->boolean('subscription_edit')->default(false);
            $table->boolean('user_create')->default(false);
            $table->boolean('user_edit')->default(false);
            $table->boolean('role_management')->default(false);
            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_role_id_foreign');
        });
        Schema::dropIfExists('roles');
    }
}
