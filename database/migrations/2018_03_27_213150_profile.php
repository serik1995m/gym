<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Profile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('height')->nullable();
            $table->integer('weight')->nullable();
            $table->string('sport')->nullable();
            $table->string('sex')->nullable();
            $table->integer('age')->nullable();
            $table->dateTime('birthday')->nullable();
            $table->string('hobby')->nullable();
            $table->string('job')->nullable();
            $table->string('photo')->nullable();
            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreign('profile_id')->references('id')->on('profiles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_profile_id_foreign');
        });
        Schema::dropIfExists('profiles');
    }
}
