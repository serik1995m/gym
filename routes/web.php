<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('admin')->group(function () {

    Route::get('/', 'AdminHomeController@index')->name('admin.home');
    
    Route::get('/types', 'TypeController@index')->name('admin.types.index');
    Route::get('/type/create', 'TypeController@create')->name('admin.type.create');
    Route::post('/type/save', 'TypeController@save')->name('admin.type.save');
    Route::get('/type/edit/{id}', 'TypeController@edit')->name('admin.type.edit');
    Route::post('/type/update/', 'TypeController@update')->name('admin.type.update');
    Route::get('/type/delete/{id}', 'TypeController@delete')->name('admin.type.delete');


    Route::get('/subscriptions', 'SubscriptionController@index')->name('admin.subscriptions.index');
    Route::get('/subscription/create', 'SubscriptionController@create')->name('admin.subscription.create');
    Route::post('/subscription/save', 'SubscriptionController@save')->name('admin.subscription.save');
    Route::get('/subscription/edit/{id}', 'SubscriptionController@edit')->name('admin.subscription.edit');
    Route::post('/subscription/update/', 'SubscriptionController@update')->name('admin.subscription.update');
    Route::get('/subscription/delete/{id}', 'SubscriptionController@delete')->name('admin.subscription.delete');


    Route::get('/athletes', 'AthleteController@index')->name('admin.athletes.index');
    Route::get('/athlete/create', 'AthleteController@create')->name('admin.athlete.create');
    Route::get('/athlete/edit/{id}', 'AthleteController@edit')->name('admin.athlete.edit');
    Route::post('/athlete/save', 'AthleteController@save')->name('admin.athlete.save');
});