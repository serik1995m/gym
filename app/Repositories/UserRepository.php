<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository
{
    public function all(array $filters = [], $perPage = null)
    {
        return User::filter($filters)->paginateFilter($perPage);
    }
}