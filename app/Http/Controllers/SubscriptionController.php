<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubscriptionRequest;
use App\Models\Subscription;
use App\Models\Type;
use \Session;

class SubscriptionController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.subscription.index', [
            'subscriptions_active' => Subscription::all()->where('status', Subscription::ACTIVE),
            'subscriptions_not_active' => Subscription::all()->where('status', Subscription::NOT_ACTIVE),
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $types = Type::all()->where('status', Type::ACTIVE);

        return view('admin.subscription.form',
            [
                'types' => $types
            ]);
    }


    public function save(SubscriptionRequest $request)
    {
        $subscription = Subscription::updateOrCreate(['id' => $request->id ], $request->all());

        if($subscription)
            Session::flash('success', 'Сохраненно!');
        else
            Session::flash('error', 'Ошибка!');

        return redirect(route('admin.subscriptions.index'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $subscription = Subscription::findOrFail($id);
        $types = Type::all()->where('status', Type::ACTIVE);

        return view('admin.subscription.form', [
            'subscription' => $subscription,
            'types' => $types
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        $subscription = Subscription::findOrFail($id);

        if($subscription->delete())
            Session::flash('success', 'Удаленно!');
        else
            Session::flash('error', 'Ошибка!');

        return redirect(route('admin.subscriptions.index'));
    }
}
