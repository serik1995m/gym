<?php

namespace App\Http\Controllers;

use App\Http\Requests\TypeRequest;
use App\Models\Type;
use Illuminate\Http\Request;
use \Session;

class TypeController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.type.index', [
            'types_active' => Type::all()->where('status', '=', true),
            'types_not_active' => Type::all()->where('status', '=', false),
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.type.create');
    }

    public function save(TypeRequest $request)
    {
        $type = Type::create($request->all());

        if($type)
            Session::flash('success', 'Тип добавлен!');
        else
            Session::flash('error', 'Ошибка! Тип не добавлен!');

        return redirect(route('admin.types.index'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {
        return view('admin.type.edit', ['type' => Type::findOrFail($request->id)]);
    }

    /**
     * @param TypeRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(TypeRequest $request)
    {

        $update = Type::findOrFail($request->id)->update($request->all());

        if($update)
            Session::flash('success', 'Тип отредактирован!');
        else
            Session::flash('error', 'Ошибка редактирования!');

        return redirect(route('admin.types.index'));
    }


    public function delete(Request $request)
    {
        $delete = Type::findOrFail($request->id)->delete();

        if($delete)
            Session::flash('success', 'Тип удален!');
        else
            Session::flash('error', 'Ошибка! Тип не удален!');

        return redirect(route('admin.types.index'));
    }
}
