<?php

namespace App\Http\Controllers;

use App\Http\Requests\AthleteRequest;
use App\Http\Requests\AthleteSearchRequest;
use App\Models\User;
use App\Repositories\UserRepository;
use App\Models\Roles;
use Illuminate\Support\Facades\Hash;
use \Session;

class AthleteController extends Controller
{
    protected $athlete;

    /**
     * AthleteController constructor.
     * @param UserRepository $athlete
     */
    public function __construct(UserRepository $athlete)
    {
        $this->athlete = $athlete;
    }

    /**
     * @param AthleteSearchRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(AthleteSearchRequest $request)
    {
        return view('admin.athlete.index', [
            'athletes' => $this->athlete->all($request->all(), 20)
        ]);
    }

    public function create()
    {
        return view('admin.athlete.form');
    }

    public function edit($id)
    {
        return view('admin.athlete.form', [
            'athlete' => User::findOrFail($id),
            'roles' => Roles::all()
        ]);
    }

    public function save(AthleteRequest $request)
    {
        $data = array_merge($request->all(), ['password' => Hash::make($request->phone)]);

        $athlete = User::updateOrCreate(['id' => $request->id], $data);


        if ($athlete)
            Session::flash('success', 'Сохраненно!');
        else
            Session::flash('error', 'Ошибка!');

        return redirect(route('admin.athletes.index'));
    }
}
