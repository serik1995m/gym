<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubscriptionRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type_id' => 'exists:types,id',
            'name' => 'string',
            'price' => 'nullable|integer',
            'count_days' => 'nullable|integer',
            'number_trainings' => 'nullable|integer',
            'status' => 'integer'
        ];
    }
}
