<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AthleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string',
            'surname' => 'string',
            'patronymic' => 'string',
            'email' => 'unique:users,email|email|required',
            'phone' => 'unique:users,phone|required|string',
            'password' => 'string',
        ];
    }
}
