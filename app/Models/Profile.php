<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profiles';

    protected $filable = [
        'height',
        'weight',
        'sport',
        'sex',
        'age',
        'birthday',
        'hobby',
        'job',
        'photo'
    ];

    public function user()
    {
        return $this->hasOne('App\Models\User');
    }
}
