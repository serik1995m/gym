<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    const ACTIVE = true;
    const NOT_ACTIVE = false;

    protected $fillable = [
        'name',
        'description',
        'status',
    ];
}
