<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    const ACTIVE = true;
    const NOT_ACTIVE = false;

    protected $fillable = [
        'type_id',
        'name',
        'price',
        'count_days',
        'number_trainings',
        'status',
    ];
}
