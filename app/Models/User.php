<?php

namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use Filterable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id',
        'profile_id',
        'name',
        'surname',
        'patronymic',
        'email',
        'phone',
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];


    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }

    public function role()
    {
        return $this->belongsTo(Roles::class);
    }
}
