<?php

namespace app\Models\Filters;

use EloquentFilter\ModelFilter;

class UserFilter extends ModelFilter
{
    public function id($value)
    {
        $this->where(__FUNCTION__, $value);
    }

    public function name($value)
    {
        $this->whereLike(__FUNCTION__, $value);
    }

    public function surname($value)
    {
        $this->whereLike(__FUNCTION__, $value);
    }

    public function patronymic($value)
    {
        $this->whereLike(__FUNCTION__, $value);
    }

    public function email($value)
    {
        $this->whereLike(__FUNCTION__, $value);
    }

    public function phone($value)
    {
        $this->whereLike(__FUNCTION__, $value);
    }
}